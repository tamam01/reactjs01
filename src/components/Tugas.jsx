import React from 'react';
import logo from '../logo.svg';
import './Tugas.css';


function Tugas(props) {
    return (
    <div className="Tugas">
        <img src={logo} className="Tugas-logo" alt="logo" />
        <h1>Selamat Datang Di Aplikasi Reacts, {props.name}!</h1>
        <p>Silahkan Masukkan Username dan Password Anda di bawah ini.</p>
    </div>
    );
  }
  
export default Tugas;