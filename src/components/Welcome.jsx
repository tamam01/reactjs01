import React from 'react';
import './Welcome.css';

class Welcome extends React.Component{
    render(){
        return(
            <h1>Halo, Selamat Datang {this.props.nama}</h1>
        )
    }
}

export default Welcome
