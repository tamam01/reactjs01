import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Tugas from './components/Tugas';
import TugasForm from './components/TugasForm';
import TugasButton from './components/TugasButton';
import * as serviceWorker from './serviceWorker';





ReactDOM.render(
  <React.StrictMode>
     {
    
    <div>
    <Tugas name="Badrud Tamam" />
    <TugasForm masukan="Username" />
    <TugasForm masukan="Password" />
    <div className="flex-container">
    <TugasButton button="Login" />
    
    </div>
    </div>
    }
  </React.StrictMode>, document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
